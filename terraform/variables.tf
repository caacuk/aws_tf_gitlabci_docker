variable "aws_access_key" {
    default = ""
}

variable "aws_secret_key" {
    default = ""
}

variable "region" {
    default = "ap-southeast-1"
}

variable "image_id" {
    default = "ami-055147723b7bca09a"
}

variable "instance_type" {
    default = "t2.medium"
}

variable "availability_zones" {
    default = ["ap-southeast-1a"]
}

variable "max_size" {
    default = 5
}

variable "min_size" {
    default = 2
}

variable "desired_capacity" {
    default = 3
}

variable "key_name" {
    default = "aws_server"
}

variable "vpc_cidr_block" {
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr_block" {
    default = "10.0.0.0/24"
}

variable "private_subnet_cidr_block" {
    default = "10.0.1.0/24"
}
terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
        }
    }
    required_version = ">= 0.14.9"
}

provider "aws" {
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key
    profile = "default"
    region = var.region
}

/* VPC */
resource "aws_vpc" "jati_vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "jati_vpc"
  }
}

/* Public Subnet */
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.jati_vpc.id
  cidr_block = var.public_subnet_cidr_block
  map_public_ip_on_launch = true
  tags = {
    Name = "public"
  }
}

/* Private Subnet */
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.jati_vpc.id
  cidr_block = var.private_subnet_cidr_block
  map_public_ip_on_launch = false
  tags = {
    Name = "private"
  }
}

/* Internet gateway for the public subnet */
resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.jati_vpc.id
  tags = {
    Name = "ig1"
  }
}

/* Elastic IP for NAT */
resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.ig]
  tags = {
    Name = "nat_eip"
  }
}

/* NAT */
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public.id
  depends_on    = [aws_internet_gateway.ig]
  
  tags = {
    Name = "nat"
  }
}

/* Routing table for private subnet */
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.jati_vpc.id
  tags = {
    Name = "rt_private"
  }
}

/* Routing table for public subnet */
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.jati_vpc.id
  tags = {
    Name = "rt_public"
  }
}

/* Routing for public subnet */
resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ig.id
}

/* Routing for private subnet */
resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

/* Route table associations for public subnet*/
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

/* Route table associations for private subnet*/
resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

/* VPC's Default Security Group */
resource "aws_security_group" "default" {
  name        = "default-sg"
  vpc_id      = aws_vpc.jati_vpc.id
  depends_on  = [aws_vpc.jati_vpc]
  ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = true
  }
  
  egress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    self      = "true"
  }
}

/* Launch Template */
resource "aws_launch_template" "template" {
    name = "template"
    image_id      = var.image_id
    instance_type = var.instance_type
    key_name      = var.key_name
    tags = {
        Name = "jati_instance"
    }
}

/* Autoscaling Group */
resource "aws_autoscaling_group" "as_group" {
    name               = "as_group"
    desired_capacity   = var.desired_capacity
    max_size           = var.max_size
    min_size           = var.min_size
    vpc_zone_identifier = [ aws_subnet.private.id ]

    launch_template {
        id      = aws_launch_template.template.id
        version = "$Latest"
    }
}

/* Autoscaling Group Policy */
resource "aws_autoscaling_policy" "as_policy" {
  name                   = "as_policy"
  adjustment_type        = "ChangeInCapacity"
  policy_type            = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.as_group.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 45.0
  }
}